const tree = require('../tree/tree.js');
const tree_loader = require('../tree/tree_loader.js');

module.exports = ({ router }) => {
  router
    .get('/', async ctx => {
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(await tree_loader.loadTreeFromAPI('https://random-tree.herokuapp.com/'));
    })
    .get('/parent/:id', async ctx => {
      const random_tree = await tree_loader.loadTreeFromAPI('https://random-tree.herokuapp.com/');
      const list = tree.getParentList(random_tree, ctx.params.id);
      let result = undefined;
      if (list){
        result = {
          parent:list,
          tree:random_tree
        };
      }
      else{
        ctx.status = 404;
        result = {
          error:1,
          description:"No such node"
        }
      }
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(result);
    })
    .get('/child/:id', async ctx => {
      const random_tree = await tree_loader.loadTreeFromAPI('https://random-tree.herokuapp.com/');
      const list = tree.getChildList(random_tree, ctx.params.id);
      let result = undefined;
      if (list){
        result = {
          child:list,
          tree:random_tree
        };
      }
      else{
        ctx.status = 404;
        result = {
          error:1,
          description:"No such node"
        }
      }
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(result);
    })
};