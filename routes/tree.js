const tree = require('../tree/tree.js');
const tree_loader = require('../tree/tree_loader.js');
const tree_cache = require('../tree/tree_cache.js');

function getTreeStuff(tree_id, node_fn, node_fn_args, node_key){
  const cached_tree = tree_cache.getTree(tree_id);
  let result = {}, status = 200;
  if (cached_tree){
    if (node_fn) {
      const stuff = node_fn.apply(null, [cached_tree, ...node_fn_args]);
      if (stuff) {
        result[node_key] = stuff
      }
      else {
        status = 404;
        result.error = {code: 2, description: "No such node"};
      }
    }
    result.tree_id = tree_id;
    result.tree = cached_tree;
  }
  else{
    status = 404;
    result.error = {code:1,description:"No such tree"};
  }
  return {status, result};
}

module.exports = ({ treeRouter }) => {
  treeRouter
    .get('/', async ctx => {
      const new_tree = await tree_loader.loadTreeFromAPI('https://random-tree.herokuapp.com/');
      const id = tree_cache.addTree(new_tree);
      const result = {
        tree_id:id,
        tree:new_tree
      };
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(result);
    })
    .get('/:id', ctx => {
      const stuff = getTreeStuff(ctx.params.id);
      ctx.status = stuff.status;
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(stuff.result);
    })
    .get('/:id/node/:node_id', ctx => {
      const stuff = getTreeStuff(ctx.params.id, tree.getNode, [ctx.params.node_id], 'node');
      ctx.status = stuff.status;
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(stuff.result);
    })
    .get('/:id/node/:node_id/parents', ctx => {
      const stuff = getTreeStuff(ctx.params.id, tree.getParentList, [ctx.params.node_id], 'parents');
      ctx.status = stuff.status;
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(stuff.result);
    })
    .get('/:id/node/:node_id/children', ctx => {
      const stuff = getTreeStuff(ctx.params.id, tree.getChildList, [ctx.params.node_id], 'children');
      ctx.status = stuff.status;
      ctx.type = 'application/json';
      ctx.body = JSON.stringify(stuff.result);
    })
};