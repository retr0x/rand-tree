const server = require("../index");
const request = require("supertest");
const tree_loader = require('../tree/tree_loader.js');

afterEach(() => {
  server.close();
});

describe("routes: /tree", () => {
  it("should return a tree as expected and its id", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/tree");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.tree).toEqual(mockVal);
    expect(response.body.tree_id).toBeTruthy();
  });

  it("should return the tree by its id", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/tree");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.tree).toEqual(mockVal);
    expect(response.body.tree_id).toBeTruthy();

    const response2 = await request(server).get("/tree/"+response.body.tree_id);
    expect(response2.status).toEqual(200);
    expect(response2.type).toEqual("application/json");
    expect(response2.body.tree).toEqual(mockVal);
    expect(response2.body.tree_id).toEqual(response.body.tree_id);
  });

  it("should return the node of a tree by its id", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/tree");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.tree).toEqual(mockVal);
    expect(response.body.tree_id).toBeTruthy();

    const response2 = await request(server).get("/tree/"+response.body.tree_id+'/node/23');
    expect(response2.status).toEqual(200);
    expect(response2.type).toEqual("application/json");
    expect(response2.body.tree).toEqual(mockVal);
    expect(response2.body.tree_id).toEqual(response.body.tree_id);
    expect(response2.body.node).toEqual({
      "id": 23,
      "child": [
        {
          "id": 24,
          "child": [
            {
              "id": 25,
              "child": []
            }
          ]
        }
      ]
    });
  });

  it("should return the parents of a node of a tree", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/tree");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.tree).toEqual(mockVal);
    expect(response.body.tree_id).toBeTruthy();

    const response2 = await request(server).get("/tree/"+response.body.tree_id+'/node/23/parents');
    expect(response2.status).toEqual(200);
    expect(response2.type).toEqual("application/json");
    expect(response2.body.tree).toEqual(mockVal);
    expect(response2.body.tree_id).toEqual(response.body.tree_id);
    expect(response2.body.parents).toEqual(Array.from([1,16,22]));
  });

  it("should return the children of a node of a tree", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/tree");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.tree).toEqual(mockVal);
    expect(response.body.tree_id).toBeTruthy();

    const response2 = await request(server).get("/tree/"+response.body.tree_id+'/node/23/children');
    expect(response2.status).toEqual(200);
    expect(response2.type).toEqual("application/json");
    expect(response2.body.tree).toEqual(mockVal);
    expect(response2.body.tree_id).toEqual(response.body.tree_id);
    expect(response2.body.children).toEqual(Array.from([24,25]));
  });
});