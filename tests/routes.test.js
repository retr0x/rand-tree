const server = require("../index");
const request = require("supertest");
const tree_loader = require('../tree/tree_loader.js');

afterEach(() => {
  server.close();
});

describe("routes: index", () => {
  it("should return a tree as expected", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body).toEqual(mockVal);
  });

  it("should return the list of children as expected", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/child/23");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.child).toEqual(Array.from([24,25]));
    expect(response.body.tree).toEqual(mockVal);
  });

  it("should return the list of parents as expected", async () => {
    const mockVal = await tree_loader.loadTreeFromFile('./tests/random-tree.json');
    tree_loader.loadTreeFromAPI = jest.fn().mockResolvedValue(mockVal);

    const response = await request(server).get("/parent/23");
    expect(response.status).toEqual(200);
    expect(response.type).toEqual("application/json");
    expect(response.body.parent).toEqual(Array.from([1,16,22]));
    expect(response.body.tree).toEqual(mockVal);
  });
});