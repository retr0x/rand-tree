const tree_cache = require('../tree/tree_cache');

describe("tree cache:", () => {
  it("should start with 0 items", () => {
    expect(tree_cache.getNumTrees()).toEqual(0);
  });

  it("should have 1 item after adding the first tree and store it", () => {
    const id = tree_cache.addTree('test_tree');
    expect(tree_cache.getNumTrees()).toEqual(1);
    expect(tree_cache.getTree(id)).toEqual('test_tree');
  });

  it("should cache items according to FIFO rules", () => {
    const ids = [];
    for (let i=0; i<2*tree_cache.getCacheSize(); i++){
      ids[i] = tree_cache.addTree('tree_'+i);
    }
    expect(tree_cache.getNumTrees()).toEqual(tree_cache.getCacheSize());

    for (let i=0; i<tree_cache.getCacheSize(); i++){
      expect(tree_cache.getTree(ids[i])).toEqual(undefined);
    }
    for (let i=tree_cache.getCacheSize(); i<2*tree_cache.getCacheSize(); i++){
      expect(tree_cache.getTree(ids[i])).toEqual('tree_'+i);
    }
  });
});