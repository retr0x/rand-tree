function logNode(node, level = 0){
  console.log(" ".repeat(level)+node.id);
  node.child.forEach(child => logNode(child, level+1));
}

function findNode(node, id, path = []){
  if(node.id === id){
    return {node:node, path:Array.from(path)};
  }
  if(node.child && node.child.length > 0){
    let found = false, i = 0;
    path.push(node.id);
    do{
      found = findNode(node.child[i++], id, path);
    }
    while (!found && i<node.child.length);
    path.pop();
    return found;
  }
  return false;
}

function listChildNodes(node, child_list = []){
  if(node.child && node.child.length>0){
    node.child.forEach(child => {
      child_list.push(child.id);
      listChildNodes(child).forEach(el => child_list.push(el));
    });
  }
  return child_list;
}

function getParentList(tree, node_id){
  const node = findNode(tree, parseInt(node_id));
  if (node){
    return node.path;
  }
}

function getChildList(tree, node_id){
  const node = findNode(tree, parseInt(node_id));
  if (node){
    return listChildNodes(node.node);
  }
}

function getNode(tree, node_id){
  const node = findNode(tree, parseInt(node_id));
  if (node){
    return node.node;
  }
}

module.exports = {
  "getParentList":getParentList,
  "getChildList":getChildList,
  "getNode":getNode
};