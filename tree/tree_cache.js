const uuidv4 = require('uuid/v4');

const g_trees = new Map();
const g_tree_cache_size = 2;

function deleteCacheEntry(index){
  const it = g_trees.entries();

  let entry, i=0;
  do {
    entry= it.next();
    i++;
  }
  while(!entry.done && i<index);

  if (!entry.done){
    g_trees.delete(entry.value[0]);
  }
}

function addTree(tree){
  if(tree){
    const id = uuidv4();

    if(g_trees.size >= g_tree_cache_size){
      deleteCacheEntry(0);
    }

    g_trees.set(id, tree);
    return id;
  }
}

function getTree(id){
  if(id){
    return g_trees.get(id);
  }
}

function getNumTrees(){
  return g_trees.size;
}

function getCacheSize(){
  return g_tree_cache_size;
}

module.exports = {
  "addTree":addTree,
  "getTree":getTree,
  "getNumTrees":getNumTrees,
  "getCacheSize":getCacheSize
};