const loadJsonFile = require('load-json-file');
const http = require('https');

async function loadTreeFromFile(filename){
  return await loadJsonFile(filename);
}

function fetchJSONDataFromAPI(url){
  return new Promise((resolve,reject) => {
    http.get(url, res => {
      const {statusCode} = res;
      const contentType = res.headers['content-type'];

      let error;
      if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
          `Status Code: ${statusCode}`);
      } else if (!/^application\/json/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
          `Expected application/json but received ${contentType}`);
      }
      if (error) {
        reject(error.message);
        // consume response data to free up memory
        res.resume();
        return;
      }

      res.setEncoding('utf8');
      let rawData = '';
      res.on('data', (chunk) => {
        rawData += chunk;
      });
      res.on('end', () => {
        try {
          const parsedData = JSON.parse(rawData);
          resolve(parsedData);
        } catch (e) {
          reject(e.message);
        }
      });
    }).on('error', (e) => {
      reject(`Got error: ${e.message}`);
    });
  });
}

async function loadTreeFromAPI(url){
  return await fetchJSONDataFromAPI(url);
}

module.exports = {
  "loadTreeFromFile": loadTreeFromFile,
  "loadTreeFromAPI": loadTreeFromAPI,
};