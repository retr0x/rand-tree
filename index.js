'use strict';

const koa = require('koa');
const Router = require('koa-router');
const logger = require('koa-logger');

const app = new koa();

app.use(logger());

// error handling
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = '['+ctx.status+'] '+err.message;
    ctx.app.emit('error', err, ctx);
  }
});


const router = new Router();
const treeRouter = new Router({prefix:'/tree'});
require('./routes/index.js')({ router });
require('./routes/tree.js')({ treeRouter });

app
  .use(router.routes())
  .use(router.allowedMethods())
  .use(treeRouter.routes())
  .use(treeRouter.allowedMethods());

const server = app.listen(process.env.PORT || 3000, () => {
  console.log('listening on port', process.env.PORT|| 3000);
});

module.exports = server;